# Bangla Web Tools
_Easiest way to type Bangla on any computer in the world, on any browser!_

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

To use this, visit*: [https://adnan360.gitlab.io/bnwebtools](https://adnan360.gitlab.io/bnwebtools) \
or, [the original (but non-HTTPS) version](http://bnwebtools.sourceforge.net/)
or clone this repo and open `public/index.html` in a webbrowser.

Lets you type Bangla (or Bengali) using popular keyboard layouts, such as, Bijoy, Somewherein, Avro, Unijoy. Plus, it can convert text from old ASCII based systems, such as, Bijoy, Somewherein, Boishakhi etc. to Unicode text.

Setting up keyboards has been getting easier everyday on newer Operating Systems. But sometimes we don't have the time to set it up (especially if you just setup a new computer/don't have admin privileges to install keyboards and want to write down something quickly). This is an excellent site to use without installing anything on your computer. Any modern browser will do.

<span>*</span> [GitLab Terms of Service](https://about.gitlab.com/terms/) should apply.


## Origin

This repo is forked from the excellent [Bangla Web Tools](https://sourceforge.net/projects/bnwebtools/) project on SourceForge by [udvranto](http://sourceforge.net/users/udvranto). Loads of thanks to him for making this available in GPLv2.


## Improvements/Changes

- Updated to 2.1.0 Alpha 1 (September 18, 2007) from the [website](http://bnwebtools.sourceforge.net/) - the version downloadable from [SourceForge](https://sourceforge.net/projects/bnwebtools/) is 1.5.3
- `index.html`: Autoconverted some tags to lowercase, fixed tag hierarchy etc. through saving with IceWeasel browser.
- `index.html`: Removed Google Analytics and BraveNet code for privacy reasons.
- `index.html`: Added copyright notice, GPL text and original author details into index.html.
- Added `resources`.


## License

GNU General Public License version 2 (See `LICENSE` for details).
